using TMPro;
using UnityEngine;

public class MeasureTool : MonoBehaviour
{
    #region Serialised fields

    [SerializeField]
    [Tooltip("Canvas on which arrows and text will be drawn")]
    private Canvas _canvas;

    #endregion
    
    #region Serialized fields associated with arrows

    [Header("======= Arrows parameters =======")]
    [SerializeField]
    [Tooltip("Object representing right arrow")]
    private GameObject _rightArrow;
    
    [SerializeField]
    [Tooltip("Object representing left arrow")]
    private GameObject _leftArrow;

    [SerializeField]
    [Range(0.01F, 5.00F)]
    [Tooltip("Scale of the arrows")]
    private float _arrowsScale = 1.0F;

    [SerializeField]
    [Tooltip("Color of the arrows")]
    private Color _arrowsColor;
    
    [SerializeField]
    [Range(0.0F, 180.0F)]
    [Tooltip("Angle of the arrows")]
    private float _arrowsAngle = 0.0F;
    
    #endregion

    #region Serialised field associated with text between arrows

    [Header("======= Text parameters =======")]
    [SerializeField]
    [Tooltip("Text displayed between arrows")]
    private TMP_Text _text;
    
    [SerializeField]
    [Range(0.0001F, 5.00F)]
    [Tooltip("Scale of the text displayed between arrows")]
    private float _textScale = 1.0F;

    [SerializeField]
    [Tooltip("Color of the text displayed between arrows")]
    private Color _textColor;
    
    [SerializeField]
    [Range(0.0F, 180.0F)]
    [Tooltip("Angle of the arrows")]
    private float _textAngle = 0.0F;

    [SerializeField]
    [Range(-1.0F, 1.0F)]
    [Tooltip("Vertical offset of the text from the center")]
    private float _textVerticalOffset = 0.5F;
    
    [SerializeField]
    [Range(-1.0F, 1.0F)]
    [Tooltip("Horizontal offset of the text from the center")]
    private float _textHorizontalOffset = 0.0F;
    
    [SerializeField]
    [Range(0.0F,1.0F)]
    [Tooltip("Orientation of the text between arrows (0 - left side, 0.5 center, 1 - right side)")]
    private float _textOrientation = 0.5F;

    [SerializeField]
    [Range(0,5)]
    [Tooltip("Number of decimal places to display on measured distance")]
    private int _numberOfDecimalPlaces = 2;

    [SerializeField]
    [Tooltip("Default measure unit used in project by Unity")]
    private MeasureUnit _defaultMeasureUnitUsedInProject = MeasureUnit.M;
    
    [SerializeField]
    [Tooltip("Measure units (millimeter, centimeter, meter, kilometer)")]
    private MeasureUnit _measureUnit = MeasureUnit.CM;

    #endregion

    #region Serialised fields associated with line

    [Header("======= Line parameters =======")]
    [SerializeField]
    [Tooltip("Line game object drawn between arrows")]
    private GameObject _line;

    [SerializeField]
    [Tooltip("Flag if line should be draw")]
    private bool _drawLine = true;

    [SerializeField]
    [Tooltip("Color of the line")]
    private Color _lineColor;

    [SerializeField]
    [Range(0.01F, 1.0F)]
    [Tooltip("Thickness of the line")]
    private float _lineThickness = 0.02F;
    
    #endregion

    #region Private fields

    /// <summary>
    ///   Measure units (millimeter, centimeter, meter, kilometer).
    /// </summary>
    private enum MeasureUnit
    {
        MM = 1,
        CM = 10,
        M = 1000,
        KM = 1000000
    }

    /// <summary>
    ///   Measure unit multiplier.
    /// </summary>
    private float _measureUnitMultiplier;

    /// <summary>
    ///   Sprite renderer of left the arrow.
    /// </summary>
    private SpriteRenderer _leftArrowSpriteRenderer;
    
    /// <summary>
    ///   Sprite renderer of right the arrow.
    /// </summary>
    private SpriteRenderer _rightArrowSpriteRenderer;

    /// <summary>
    ///   Line renderer.
    /// </summary>
    private LineRenderer _lineRenderer;

    /// <summary>
    ///   Mesh renderer of the measured object.
    /// </summary>
    private MeshRenderer _measuredObjectMeshRenderer;

    #endregion

    #region Private methods

    /// <summary>
    ///   Awake.
    /// </summary>
    private void Awake()
    {
        GetComponents();
        CalculateUnitMeasureMultiplier();
        DisplayMeasurement();
    }

    /// <summary>
    ///   Get needed components used by this scripts.
    /// </summary>
    private void GetComponents()
    {
        _leftArrowSpriteRenderer = _leftArrow.GetComponent<SpriteRenderer>();
        _rightArrowSpriteRenderer = _rightArrow.GetComponent<SpriteRenderer>();
        _lineRenderer = _line.GetComponent<LineRenderer>();
    }
    
    /// <summary>
    ///   Calculate multiplier of unit measure.
    /// </summary>
    private void CalculateUnitMeasureMultiplier()
    {
        _measureUnitMultiplier = (float)(int)_defaultMeasureUnitUsedInProject / (int)_measureUnit;
    }
    
    /// <summary>
    ///   Display distance.
    /// </summary>
    private void DisplayMeasurement()
    {
        UpdateArrows();
        UpdateText();
        UpdateLine();
    }

    /// <summary>
    ///   OnValidate - executed when something any exposed field on script was changed.
    /// </summary>
    private void OnValidate()
    {
        // OnValidate() must be used to easy refresh data inside Unity Editor if smth changed on the script(eg. measure unit was changed). 
        Awake();
    }

    /// <summary>
    ///   Show measure tool inside Unity Editor.
    /// </summary>
    private void OnDrawGizmos()
    {
        Awake();
    }

    #endregion

    #region Arrows

    /// <summary>
    ///   Update arrows.
    /// </summary>
    private void UpdateArrows()
    {
        SetArrowsColor();
        SetArrowsScale();
        SetArrowsAngle();
    }

    /// <summary>
    ///   Set colors of the arrows.
    /// </summary>
    private void SetArrowsColor()
    {
        _leftArrowSpriteRenderer.color = _arrowsColor;
        _rightArrowSpriteRenderer.color = _arrowsColor;
    }

    /// <summary>
    ///   Set scale of the arrows.
    /// </summary>
    private void SetArrowsScale()
    {
        _leftArrow.transform.localScale = new Vector3(_arrowsScale,_arrowsScale, _arrowsScale);
        _rightArrow.transform.localScale = new Vector3(_arrowsScale,_arrowsScale, _arrowsScale);
    }

    /// <summary>
    ///   Set angle of the arrows.
    /// </summary>
    private void SetArrowsAngle()
    {
        Vector3 rot = _leftArrow.transform.localRotation.eulerAngles;
        _leftArrow.transform.localRotation = Quaternion.Euler(_arrowsAngle, rot.y, rot.z);
         rot = _rightArrow.transform.localRotation.eulerAngles;
         _rightArrow.transform.localRotation = Quaternion.Euler(_arrowsAngle, rot.y, rot.z);
    }

    #endregion

    #region Text

    /// <summary>
    ///   Update text.
    /// </summary>
    private void UpdateText()
    {
        SetDistanceText(GetDistance());
        SetTextColor();
        SetTextScale();
        SetTextAngle();
        UpdateCanvasPosition();
    }

    /// <summary>
    ///   Measure distance between arrows.
    /// </summary>
    private float GetDistance()
    {
        float distance = Vector3.Distance(_rightArrow.transform.position, _leftArrow.transform.position);
        return distance * _measureUnitMultiplier;
    }

    /// <summary>
    ///   Set distance text.
    /// </summary>
    /// <param name="distance">Measured distance</param>
    private void SetDistanceText(float distance)
    {
        string distanceWithMeasureUnitText = $"{string.Format($"{{0:F{_numberOfDecimalPlaces}}}", distance)} {_measureUnit}";
        _text.SetText(distanceWithMeasureUnitText);
    }
     
    /// <summary>
    ///   Set colors of the text.
    /// </summary>
    private void SetTextColor()
    {
        _text.color = _textColor;
    }

    /// <summary>
    ///   Set scale of the text.
    /// </summary>
    private void SetTextScale()
    {
        _text.transform.localScale = new Vector3(_textScale,_textScale, _textScale);
    }

    /// <summary>
    ///   Set angle of the text.
    /// </summary>
    private void SetTextAngle()
    {
        Vector3 rot = _text.transform.localRotation.eulerAngles;
        _text.transform.localRotation = Quaternion.Euler(_textAngle, rot.y, rot.z);
    }
    
    /// <summary>
    ///   Update canvas position based on arrow positions.
    /// </summary>
    private void UpdateCanvasPosition()
    {
        Vector3 position = _leftArrow.transform.position + (_textOrientation * (_rightArrow.transform.position - _leftArrow.transform.position));
        _canvas.transform.position = position + new Vector3(_textHorizontalOffset, _textVerticalOffset, 0.0F);
    }

    #endregion

    #region Line

    /// <summary>
    ///   Update line.
    /// </summary>
    private void UpdateLine()
    {
        ToggleLine();
        if(!_drawLine)
        {
            return;
        }
        
        SetLineColor();
        SetLineThickness();
        SetLinePositions();;
    }

    /// <summary>
    ///   Toggle line on or off.
    /// </summary>
    private void ToggleLine()
    {
        _line.gameObject.SetActive(_drawLine);
    }
    
    /// <summary>
    ///   Set positions of the line.
    /// </summary>
    private void SetLinePositions()
    {
        if((transform.localRotation.eulerAngles.y == 0) && transform.localRotation.eulerAngles.z == 0)
        {
            _lineRenderer.SetPosition(0, _leftArrow.transform.position + new Vector3(0.0F,0.0F, _leftArrowSpriteRenderer.bounds.size.z));
            _lineRenderer.SetPosition(1, _rightArrow.transform.position - new Vector3(0.0F,0.0F,_rightArrowSpriteRenderer.bounds.size.z));
            return;
        }
        
        if((transform.localRotation.eulerAngles.y == -90 || transform.localRotation.eulerAngles.y == 270) && transform.localRotation.eulerAngles.z == 0)
        {
            if(_leftArrow.transform.position.y < 0)
            {
                _lineRenderer.SetPosition(0, _leftArrow.transform.position + new Vector3(0.0F,_leftArrowSpriteRenderer.bounds.size.y,0.0F));
                _lineRenderer.SetPosition(1, _rightArrow.transform.position - new Vector3(0.0F,_rightArrowSpriteRenderer.bounds.size.y,0.0F));
            }
            else
            {
                _lineRenderer.SetPosition(0, _leftArrow.transform.position - new Vector3(0.0F,_leftArrowSpriteRenderer.bounds.size.y,0.0F));
                _lineRenderer.SetPosition(1, _rightArrow.transform.position + new Vector3(0.0F,_rightArrowSpriteRenderer.bounds.size.y,0.0F));
            }
        }
        else
        {
            if(_leftArrow.transform.position.x < 0)
            {
                _lineRenderer.SetPosition(0, _leftArrow.transform.position + new Vector3(_leftArrowSpriteRenderer.bounds.size.x,0.0F,0.0F));
                _lineRenderer.SetPosition(1, _rightArrow.transform.position - new Vector3(_rightArrowSpriteRenderer.bounds.size.x,0.0F,0.0F));
            }
            else
            {
                _lineRenderer.SetPosition(0, _leftArrow.transform.position - new Vector3(_leftArrowSpriteRenderer.bounds.size.x,0.0F,0.0F));
                _lineRenderer.SetPosition(1, _rightArrow.transform.position + new Vector3(_rightArrowSpriteRenderer.bounds.size.x,0.0F,0.0F));
            } 
        }
    }
    
    /// <summary>
    ///   Set colors of the line.
    /// </summary>
    private void SetLineColor()
    {
        _lineRenderer.startColor = _lineColor;
        _lineRenderer.endColor = _lineColor;
    }
    
    /// <summary>
    ///   Set thickness of the line.
    /// </summary>
    private void SetLineThickness()
    {
        _lineRenderer.startWidth = _lineThickness;
        _lineRenderer.endWidth = _lineThickness;
    }

    #endregion

    public void MeasureObject(GameObject gameObject)
    {
        // _measuredObjectMeshRenderer = gameObject.GetComponent<MeshRenderer>();
        // if(_measuredObjectMeshRenderer == null)
        // {
        //     Debug.Log("No mesh renderer !!!!!!!!!!!!!!!!!!!!!!");
        //     return;
        // }
        // Debug.Log("_measuredObjectMeshRenderer.bounds.min = "+_measuredObjectMeshRenderer.bounds.min);
        // Debug.Log("_measuredObjectMeshRenderer.bounds.max = "+_measuredObjectMeshRenderer.bounds.max);
        //
        // //this.gameObject.transform.position =  
        //
        // _leftArrow.transform.position = new Vector3(_measuredObjectMeshRenderer.bounds.min.x,0.0F, 0.0F);
        // _rightArrow.transform.position = new Vector3(_measuredObjectMeshRenderer.bounds.max.x,0.0F, 0.0F);
    }
}
