using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MeasureObject : MonoBehaviour
{
    [SerializeField]
    private Transform _measureToolsParent;
    
    [SerializeField]
    private GameObject _measureToolPrefab;

    [SerializeField]
    private Camera _mainCamera;
    
    
    private static MeasureObject _instance;
    

    private void OnValidate()
    {
        if(_instance == null)
        {
            _instance = this;
        }
    }

    [MenuItem("GameObject/Create Other/Measure object")]
    private static void MeasureInHierarchy()
    {
        GameObject gameObject = Instantiate(_instance._measureToolPrefab, Selection.activeGameObject.transform.position, Quaternion.identity, _instance._measureToolsParent);
        gameObject.GetComponent<MeasureTool>().MeasureObject(Selection.activeGameObject);
    }
    
    [MenuItem("MyMenu/Measure object %g")]
    private static void Measure()
    {
        Debug.Log("Doing something with a Shortcut Key...");

       // Debug.Log("Input.mousePosition="+Input.mousePosition);
        //Debug.Log("Event.current.mousePosition="+Event.current.mousePosition);
        
        //Ray ray = _instance._mainCamera.ScreenPointToRay(Input.mousePosition);
        //Ray ray = _instance._mainCamera.ScreenPointToRay(Event.current.mousePosition);
       // Vector3 mousePosition = Event.current.mousePosition;
        //Ray ray = HandleUtility.GUIPointToWorldRay(mousePosition);
        
        Vector3 mousePos = Event.current.mousePosition;
        mousePos.y = SceneView.lastActiveSceneView.camera.pixelHeight - mousePos.y;
        Ray ray = SceneView.lastActiveSceneView.camera.ScreenPointToRay(mousePos);

        Debug.DrawRay(ray.origin, ray.direction);
        
        
        if(Physics.Raycast(ray, out RaycastHit raycastHit))
        {
            Debug.Log("D=" + raycastHit.point);
        }

    }
}
